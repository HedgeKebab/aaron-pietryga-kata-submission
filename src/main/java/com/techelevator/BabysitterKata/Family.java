package com.techelevator.BabysitterKata;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Family {
	
	private static final LocalDateTime MAX_END = LocalDate.now().plusDays(1).atTime(4, 0);
	private static final LocalDateTime MIN_START = LocalDate.now().atTime(17, 0);
	private static final LocalDateTime MIDNIGHT = LocalDate.now().plusDays(1).atTime(0, 0);
	private static final LocalDateTime NINE_PM = LocalDate.now().atTime(21, 0);
	private static final LocalDateTime TEN_PM = LocalDate.now().atTime(22, 0);
	private static final LocalDateTime ELEVEN_PM = LocalDate.now().atTime(23, 0);
	private static final int NO_VALUE = 0;
	
	private static final String FAMILY_A = "A";
	private static final String FAMILY_B = "B";
	private static final String FAMILY_C = "C";
	
	private static final int RATE_FIFTEEN = 15;
	private static final int RATE_TWENTY = 20;
	private static final int RATE_TWENTY_ONE = 21;
	private static final int RATE_EIGHT = 8;
	private static final int RATE_TWELVE = 12;
	private static final int RATE_SIXTEEN = 16;
	
	private static final int INCREMENT_ONE = 1;
	
	public double job(LocalDateTime startTime, LocalDateTime endTime, String familyName) {
		int pay = NO_VALUE;
		
		if (isTimeRangeValid(startTime, endTime)) {
			return pay;
		}
		if (startTime.getHour() != endTime.getHour()) {
			while (startTime.isBefore(endTime)) {
				if (startTime.getMinute() != endTime.getMinute()) {
					if(startTime.plusHours(INCREMENT_ONE).getHour() < endTime.getHour()) {
						pay += this.familyRates(familyName, startTime);
					}	
				}
				else {
					pay += this.familyRates(familyName, startTime);
				}
				startTime = startTime.plusHours(INCREMENT_ONE);
			}
		}
			double totalPay = pay;
			
		return totalPay;
		}
	
	public int familyRates(String familyName, LocalDateTime current) {
		int rate = NO_VALUE;
		if (isSameFamily(familyName, FAMILY_A)) {
			LocalDateTime switchOne = ELEVEN_PM;
			if (current.isBefore(switchOne)) {
				rate = RATE_FIFTEEN;
			}
			else {
				rate = RATE_TWENTY;
			}
		}
		if (isSameFamily(familyName, FAMILY_B)) {
			LocalDateTime switchOne = TEN_PM;
			LocalDateTime switchTwo = MIDNIGHT;
			if (current.isBefore(switchOne)) {
				rate = RATE_TWELVE;
			}
			else if (current.isBefore(switchTwo)) {
				rate = RATE_EIGHT;
			}
			else {
				rate = RATE_SIXTEEN;
			}
		}
		if (isSameFamily(familyName, FAMILY_C)) {
			LocalDateTime switchOne = NINE_PM;
			if (current.isBefore(switchOne)) {
				rate = RATE_TWENTY_ONE;
			}
			else {
				rate = RATE_FIFTEEN;
			}
		}
		
		return rate;
	}
	
	private boolean isSameFamily(String actualFamily, String potentialFamily) {
		return actualFamily.equals(potentialFamily);
	}
	private boolean isTimeRangeValid (LocalDateTime startTime, LocalDateTime endTime) {
		return (startTime.isAfter(endTime) || startTime.isBefore(MIN_START) || endTime.isAfter(MAX_END));
	}

}

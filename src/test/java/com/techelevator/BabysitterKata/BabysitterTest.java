package com.techelevator.BabysitterKata;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BabysitterTest {
	
	Babysitter babysitter;
	
	@Before
	public void setup() {
		babysitter = new Babysitter();
	}
	@Test
	public void verify_babysitter_accepts_a_start_time() {
		Assert.assertEquals("6:00pm", babysitter.startTime("6:00pm"));
	}
	@Test
	public void verify_babysitter_start_time_does_not_take_nontime_strings() {
		Assert.assertEquals("String provided is not is not in a am or pm time format.", babysitter.startTime("fish"));
		Assert.assertEquals("String provided is not is not in a am or pm time format.", babysitter.startTime("22"));
		Assert.assertEquals("String provided is not is not in a am or pm time format.", babysitter.startTime("X-034"));
		
	}
	
	@Test
	public void verify_babysitter_accepts_seven_pm_for_start_time() {
		Assert.assertEquals("7:00pm", babysitter.startTime("7:00pm"));
	}
	
	@Test
	public void verify_babysitter_does_not_start_before_five_pm() {
		Assert.assertEquals("Start time is outside of allowable hours.", babysitter.startTime("4:00pm"));
		Assert.assertEquals("Start time is outside of allowable hours.", babysitter.startTime("8:00am"));
		Assert.assertEquals("Start time is outside of allowable hours.", babysitter.startTime("11:00am"));
	}
	
	@Test
	public void verify_babysitter_start_time_correctly_assesses_two_digit_hours() {
		Assert.assertEquals("Start time is outside of allowable hours.", babysitter.startTime("11:00am"));
		Assert.assertEquals("11:00pm", babysitter.startTime("11:00pm"));
		Assert.assertEquals("Start time is outside of allowable hours.", babysitter.startTime("10:00am"));
		Assert.assertEquals("10:00pm", babysitter.startTime("10:00pm"));
	}
	
	@Test
	public void verify_babysitter_start_time_assess_noon_and_midnight_correctly() {
		Assert.assertEquals("Start time is outside of allowable hours.", babysitter.startTime("12:00pm"));
		Assert.assertEquals("12:00am", babysitter.startTime("12:00am"));
	}
	
	@Test
	public void verify_babysitter_can_start_after_midnight() {
		Assert.assertEquals("1:00am", babysitter.startTime("1:00am"));
		Assert.assertEquals("3:00am", babysitter.startTime("3:00am"));
	}
	
	@Test
	public void verify_babysitter_cannot_start_at_four_am() {
		Assert.assertEquals("Start time is outside of allowable hours.", babysitter.startTime("4:00am"));
	}
	
	@Test
	public void verify_minutes_are_kept_for_start_time() {
		Assert.assertEquals("5:30pm", babysitter.startTime("5:30pm"));
		Assert.assertEquals("3:59am", babysitter.startTime("3:59am"));
		Assert.assertEquals("11:45pm", babysitter.startTime("11:45pm"));
		Assert.assertEquals("5:01pm", babysitter.startTime("5:01pm"));
	}
	
	@Test
	public void verify_babysitter_accepts_an_end_time() {
		Assert.assertEquals("6:00pm", babysitter.endTime("6:00pm"));
	}
	
	@Test
	public void verify_babysitter_end_time_does_not_take_nontime_strings() {
		Assert.assertEquals("String provided is not is not in a am or pm time format.", babysitter.endTime("fish"));
		Assert.assertEquals("String provided is not is not in a am or pm time format.", babysitter.endTime("22"));
		Assert.assertEquals("String provided is not is not in a am or pm time format.", babysitter.endTime("X-034"));
		
	}
	
	@Test
	public void verify_babysitter_accepts_seven_pm_for_end_time() {
		Assert.assertEquals("7:00pm", babysitter.endTime("7:00pm"));
	}
	
	@Test
	public void verify_babysitter_must_stop_before_4_am() {
		Assert.assertEquals("End time is outside of allowable hours.", babysitter.endTime("5:00am"));
		Assert.assertEquals("End time is outside of allowable hours.", babysitter.endTime("8:00am"));
		Assert.assertEquals("End time is outside of allowable hours.", babysitter.endTime("11:00am"));
	}
	
	@Test
	public void verify_babysitter_end_time_correctly_assesses_two_digit_hours() {
		Assert.assertEquals("End time is outside of allowable hours.", babysitter.endTime("11:00am"));
		Assert.assertEquals("11:00pm", babysitter.endTime("11:00pm"));
		Assert.assertEquals("End time is outside of allowable hours.", babysitter.endTime("10:00am"));
		Assert.assertEquals("10:00pm", babysitter.endTime("10:00pm"));
	}
	
	@Test
	public void verify_babysitter_end_time_assess_noon_and_midnight_correctly() {
		Assert.assertEquals("End time is outside of allowable hours.", babysitter.endTime("12:00pm"));
		Assert.assertEquals("12:00am", babysitter.endTime("12:00am"));
	}
	
	@Test
	public void verify_babysitter_can_stop_before_midnight() {
		Assert.assertEquals("9:00pm", babysitter.endTime("9:00pm"));
		Assert.assertEquals("8:00pm", babysitter.endTime("8:00pm"));
	}
	
	@Test
	public void verify_minutes_are_kept_for_end_time() {
		Assert.assertEquals("5:30pm", babysitter.endTime("5:30pm"));
		Assert.assertEquals("3:59am", babysitter.endTime("3:59am"));
		Assert.assertEquals("11:45pm", babysitter.endTime("11:45pm"));
		Assert.assertEquals("5:01pm", babysitter.endTime("5:01pm"));
	}
	
	@Test
	public void verify_babysitter_can_work() {
		Family family = new Family();
		babysitter.endTime("4:00am");
		babysitter.startTime("5:00pm");
		String familyName ="A";
		Assert.assertTrue(babysitter.work(family, familyName) >= 0);
	}
	
	@Test
	public void verify_babysitter_only_works_for_one_family_at_a_time() {
		Family family = new Family();
		babysitter.endTime("4:00am");
		babysitter.startTime("5:00pm");
		String familyName ="A";
		Assert.assertEquals(family.job(babysitter.getStartTime(), babysitter.getEndTime(), familyName), babysitter.work(family, familyName), 0.0);
		Assert.assertEquals(190.0, babysitter.work(family, familyName), 0.0);
	}
	
	@Test
	public void verify_pay_is_accurate_based_on_family() {
		Family family = new Family();
		babysitter.endTime("4:00am");
		babysitter.startTime("5:00pm");
		String familyA ="A";
		String familyB = "B";
		String familyC = "C";
		Assert.assertEquals("babysitter and family are not returning the same value.", family.job(babysitter.getStartTime(), babysitter.getEndTime(), familyA), babysitter.work(family, familyA), 0.0);
		Assert.assertEquals("babysitter and family are not returning the same value.", family.job(babysitter.getStartTime(), babysitter.getEndTime(), familyB), babysitter.work(family, familyB), 0.0);
		Assert.assertEquals("babysitter and family are not returning the same value.", family.job(babysitter.getStartTime(), babysitter.getEndTime(), familyC), babysitter.work(family, familyC), 0.0);
	}
	
	@Test
	public void verify_set_start_time_has_same_min_start() {
		LocalDateTime startTime = LocalDate.now().atTime(12, 0);
		babysitter.setStartTime(startTime);
		Assert.assertEquals(null, babysitter.getStartTime());
	}
	
	@Test
	public void verify_set_start_time_has_same_max_end() {
		LocalDateTime startTime = LocalDate.now().plusDays(1).atTime(6, 0);
		babysitter.setStartTime(startTime);
		Assert.assertEquals(null, babysitter.getStartTime());
	}
	
	@Test
	public void verify_set_end_time_has_same_min_start() {
		LocalDateTime endTime = LocalDate.now().atTime(12, 0);
		babysitter.setEndTime(endTime);
		Assert.assertEquals(null, babysitter.getEndTime());
	}
	
	@Test
	public void verify_set_end_time_has_same_max_end() {
		LocalDateTime endTime = LocalDate.now().plusDays(1).atTime(6, 0);
		babysitter.setEndTime(endTime);
		Assert.assertEquals(null, babysitter.getEndTime());
	}
}

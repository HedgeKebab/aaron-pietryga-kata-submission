package com.techelevator.BabysitterKata;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Babysitter {
	
	private static final LocalDateTime MAX_END = LocalDate.now().plusDays(1).atTime(4, 0);
	private static final LocalDateTime MIN_START = LocalDate.now().atTime(17, 0);
	private static final String ERROR_MESSAGE_START = "Start time is outside of allowable hours.";
	private static final String ERROR_MESSAGE_END = "End time is outside of allowable hours.";
	private static final String ERROR_MESSAGE = "String provided is not is not in a am or pm time format.";
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	
	private static final int INCREMENT_ONE = 1;
	private static final int END_INT = 4;
	
	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		if (startTime.isBefore(MIN_START) || startTime.isAfter(MAX_END)) {
		}
		else {
			this.startTime = startTime;
		}
	}
	
	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		if (endTime.isBefore(MIN_START) || endTime.isAfter(MAX_END)) {
		}
		else {
			this.endTime = endTime;
		}
	}

	public String startTime(String startTime) {
		startTime = ensureStringIntegrity(startTime);
		if (startTime.isEmpty()) {
			return ERROR_MESSAGE;
		}
		startTime = hourConversionToParse(startTime);
		LocalDateTime startFull = parseTime(startTime);
		startFull = convertToTwentyFourHour(startTime,startFull);
		if (startFull.getHour() < END_INT) {
			startFull = startFull.plusDays(INCREMENT_ONE);
		}
		if (startFull.isBefore(MIN_START)) {
			return ERROR_MESSAGE_START;
		}
		this.setStartTime(startFull);
		startTime = deconvertTime(startTime);
		return startTime;
	}
	
	public String endTime(String endTime) {
		endTime = ensureStringIntegrity(endTime);
		if (endTime.isEmpty()) {
			return ERROR_MESSAGE;
		}
		endTime = hourConversionToParse(endTime);
		LocalDateTime endFull = parseTime(endTime);
		
		endFull = convertToTwentyFourHour(endTime,endFull);
		if (isEndTime(endTime, endFull)) {
			endFull = endFull.plusDays(INCREMENT_ONE);
		}
		if (endFull.isAfter(MAX_END) || endFull.isBefore(MIN_START)) {
			return ERROR_MESSAGE_END;
		}
		this.setEndTime(endFull);
		endTime = deconvertTime(endTime);
		return endTime;
	}
	
	public double work(Family family, String familyName) {
		double totalPay = family.job(this.getStartTime(), this.getEndTime(), familyName);
		return totalPay;
	}
	
	private String hourConversionToParse(String time) {
		if (time.length() == 6) {
			time = "0" + time;
		} 
		return time;
	}
	private boolean isEndTime(String time, LocalDateTime fullTime) {
		return (fullTime.getHour() < 12 && abbreviation(time).equals("am") && fullTime.getHour() != 0);
	}
	private LocalDateTime parseTime(String time) {
		LocalTime conversion =  LocalTime.parse(time.substring(0, 5));
		LocalDateTime fullTime = LocalDate.now().atTime(conversion);
		return fullTime;
	}
	
	private LocalDateTime convertToTwentyFourHour(String time, LocalDateTime fullTime) {
		if ((abbreviation(time).equals("pm") && fullTime.getHour() != 12) || (time.substring(5).contentEquals("am") && fullTime.getHour() == 12)) {
			fullTime = fullTime.plusHours(12);
		}
		return fullTime;
	}
	
	private String ensureStringIntegrity(String time) {
		int length = time.length();
		if (length < 6 || length > 7) {
			time = "";
		}
		if (time.length() > 0) {
			if (!(time.substring(length - 2).toLowerCase().equals("am") || time.substring(length - 2).toLowerCase().equals("pm"))) { 
			time = "";
			}
		}
			return time;
	}
	
	private String abbreviation(String time) {
		String abbreviation = time.substring(5);
		return abbreviation;
	}
	
	private String deconvertTime(String time) {
		if (time.substring(0, 1).equals("0")) {
			time = time.substring(1);
		}
		return time;
	}

}

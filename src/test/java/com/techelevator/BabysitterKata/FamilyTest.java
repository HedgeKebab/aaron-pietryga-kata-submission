package com.techelevator.BabysitterKata;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FamilyTest {
	
	Family family;
	
	@Before
	public void setup() {
	family = new Family();
	}
	
	@Test
	public void verify_family_has_a_job() {
		
		LocalDateTime startTime = LocalDate.now().atTime(17, 0);
		LocalDateTime endTime = LocalDate.now().atTime(17, 1);
		String familyName = "null";
		
		Assert.assertEquals(0.0, family.job(startTime, endTime, familyName), 0.0);
	}
	
	@Test
	public void verify_job_actually_gives_payment() {
		LocalDateTime startTime = LocalDate.now().atTime(17, 0);
		LocalDateTime endTime = LocalDate.now().plusDays(1).atTime(4, 0);
		String familyName = "A";
		
		Assert.assertFalse(0.0 >= family.job(startTime, endTime, familyName));
	}
	
	@Test
	public void verify_family_has_valid_time_range() {
		LocalDateTime startTime = LocalDate.now().atTime(12, 0);
		LocalDateTime startTimeAlt = LocalDate.now().atTime(18, 0);
		LocalDateTime endTime = LocalDate.now().plusDays(1).atTime(9, 0);
		LocalDateTime endTimeAlt = LocalDate.now().plusDays(1).atTime(1, 0);
		String familyName = "A";
		
		Assert.assertTrue(0.0 == family.job(startTime, endTime, familyName));
		Assert.assertTrue(0.0 == family.job(startTimeAlt, endTime, familyName));
		Assert.assertTrue(0.0 == family.job(startTime, endTimeAlt, familyName));
		Assert.assertFalse(0.0 == family.job(startTimeAlt, endTimeAlt, familyName));
	}
	@Test
	public void verify_start_must_be_before_end() {
		LocalDateTime startTime = LocalDate.now().atTime(22, 0);
		LocalDateTime endTime = LocalDate.now().atTime(18, 0);
		String familyName = "null";
		
		Assert.assertEquals(0.0, family.job(startTime, endTime, familyName), 0.0);
	}
	
	@Test
	public void verify_end_must_be_after_start() {
		LocalDateTime startTime = LocalDate.now().atTime(20, 0);
		LocalDateTime endTime = LocalDate.now().atTime(18, 0);
		String familyName = "null";
		
		Assert.assertEquals(0.0, family.job(startTime, endTime, familyName), 0.0);
	}
	
	@Test
	public void verify_pay_rate_can_change_based_on_hours() {
		LocalDateTime startTime = LocalDate.now().atTime(17, 0);
		LocalDateTime endTime = LocalDate.now().plusDays(1).atTime(4, 0);
		String familyName = "null";
		String familyA = "A";
		
		Assert.assertEquals(0.0, family.job(startTime, endTime, familyName), 0.0);
		Assert.assertEquals(190.0, family.job(startTime, endTime, familyA), 0.0);
	}
	
	@Test
	public void verify_pay_rate_changes_for_family_a_is_accurate() {
		LocalDateTime startTimeOne = LocalDate.now().atTime(17, 0);
		LocalDateTime startTimeTwo = LocalDate.now().plusDays(1).atTime(1, 0);
		LocalDateTime endTimeTwo = LocalDate.now().plusDays(1).atTime(2, 0);
		LocalDateTime endTimeOne = LocalDate.now().atTime(18, 0);
		
		Assert.assertEquals(15.0, family.job(startTimeOne, endTimeOne, "A"), 0.0);
		Assert.assertEquals(20.0, family.job(startTimeTwo, endTimeTwo, "A"), 0.0);
	}
	
	@Test
	public void verify_family_b_exists() {
		LocalDateTime startTime = LocalDate.now().atTime(17, 0);
		LocalDateTime endTime = LocalDate.now().plusDays(1).atTime(4, 0);
		String familyB = "B";
		
		Assert.assertFalse(0.0 >=  family.job(startTime, endTime, familyB));
	}
	
	@Test
	public void verify_pay_rate_changes_for_family_b_is_accurate() {
		LocalDateTime startTimeOne = LocalDate.now().atTime(21, 0);
		LocalDateTime startTimeTwo = LocalDate.now().plusDays(1).atTime(1, 0);
		LocalDateTime startTimeThree = LocalDate.now().atTime(23, 0);
		LocalDateTime endTimeThree = LocalDate.now().plusDays(1).atTime(0, 0);
		LocalDateTime endTimeTwo = LocalDate.now().plusDays(1).atTime(2, 0);
		LocalDateTime endTimeOne = LocalDate.now().atTime(22, 0);
		
		Assert.assertEquals(12.0, family.job(startTimeOne, endTimeOne, "B"), 0.0);
		Assert.assertEquals(16.0, family.job(startTimeTwo, endTimeTwo, "B"), 0.0);
		Assert.assertEquals(8.0, family.job(startTimeThree, endTimeThree, "B"), 0.0);
	}
	
	@Test
	public void verify_family_c_exists() {
		LocalDateTime startTime = LocalDate.now().atTime(17, 0);
		LocalDateTime endTime = LocalDate.now().plusDays(1).atTime(4, 0);
		String familyC = "C";
		
		Assert.assertFalse(0.0 >=  family.job(startTime, endTime, familyC));
	}
	
	@Test
	public void verify_pay_rate_changes_for_family_c_is_accurate() {
		LocalDateTime startTimeOne = LocalDate.now().atTime(17, 0);
		LocalDateTime startTimeTwo = LocalDate.now().plusDays(1).atTime(1, 0);
		LocalDateTime endTimeTwo = LocalDate.now().plusDays(1).atTime(2, 0);
		LocalDateTime endTimeOne = LocalDate.now().atTime(18, 0);
		
		Assert.assertEquals(21.0, family.job(startTimeOne, endTimeOne, "C"), 0.0);
		Assert.assertEquals(15.0, family.job(startTimeTwo, endTimeTwo, "C"), 0.0);
	}
	
	@Test
	public void verify_fractional_hours_are_not_paid() {
		LocalDateTime startTime = LocalDate.now().atTime(17, 30);
		LocalDateTime endTime = LocalDate.now().atTime(19, 0);
		LocalDateTime startTimeTwo = LocalDate.now().atTime(17, 46);
		LocalDateTime startTimeThree = LocalDate.now().atTime(17, 01);
		LocalDateTime endTimeTwo = LocalDate.now().atTime(19, 30);
		String familyA ="A";
		String familyB = "B";
		String familyC = "C";
		
		Assert.assertEquals(15.0, family.job(startTime, endTime, familyA), 0.0);
		Assert.assertEquals(12.0, family.job(startTime, endTime, familyB), 0.0);
		Assert.assertEquals(21.0, family.job(startTime, endTime, familyC), 0.0);
		Assert.assertEquals(15.0, family.job(startTimeTwo, endTime, familyA), 0.0);
		Assert.assertEquals(12.0, family.job(startTimeTwo, endTime, familyB), 0.0);
		Assert.assertEquals(21.0, family.job(startTimeTwo, endTime, familyC), 0.0);
		Assert.assertEquals(15.0, family.job(startTimeTwo, endTimeTwo, familyA), 0.0);
		Assert.assertEquals(12.0, family.job(startTimeTwo, endTimeTwo, familyB), 0.0);
		Assert.assertEquals(21.0, family.job(startTimeTwo, endTimeTwo, familyC), 0.0);
		Assert.assertEquals(30.0, family.job(startTime, endTimeTwo, familyA), 0.0);
		Assert.assertEquals(24.0, family.job(startTime, endTimeTwo, familyB), 0.0);
		Assert.assertEquals(42.0, family.job(startTime, endTimeTwo, familyC), 0.0);
		Assert.assertEquals(15.0, family.job(startTimeThree, endTime, familyA), 0.0);
		Assert.assertEquals(12.0, family.job(startTimeThree, endTime, familyB), 0.0);
		Assert.assertEquals(21.0, family.job(startTimeThree, endTime, familyC), 0.0);
		
	}
}

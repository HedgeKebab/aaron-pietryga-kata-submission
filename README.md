## Kata Babysitter

The kata was completed using Java and has all dependancies routing through Maven.
The minimum babysitter class has the work method which should be used to get the desired result, for this method to work correctly both a start time and an end time must be set using either setStartTime and setEndTime or startTime and endTime before running the work method. set methods take a LocalDateTime and the start or end Time take a string representation of a 12-hour time with abbreviation.

All tests are runnable through eclipse. The Test Class names are BabysitterTest.java and FamilyTest.java.
